public class TheNumberGameDivOne {

	static public String find(long n) {
		if(n == 1) return "Brus";
		if ((n & (n - 1)) == 0) {
			n = (long) (Math.log(n) / Math.log(2));
		}
		return n % 2 == 0 ? "John" : "Brus";
	}
}
